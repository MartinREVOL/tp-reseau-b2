from scapy.all import IP, ICMP, sr1, sr, TCP
import csv
import random

result = ""


def get_arp_table():  # Tu chope la table arp avec cette petite fonction voilà :))
    """
        Get ARP table from /proc/net/arp
    """
    with open('/proc/net/arp') as arpt:
        names = [
            'IP address', 'HW type', 'Flags', 'HW address', 'Mask', 'Device'
        ]

        reader = csv.DictReader(
            arpt, fieldnames=names, skipinitialspace=True, delimiter=' ')

        next(reader)

        return [block for block in reader]


ARPTABLE = get_arp_table()


# Normalement on est censé choper notre adresse IP sauf que bah ça marche pas donc on l'a écrit direct
IPAddr = "10.3.1.1"
port_range = []
Incr = 0
ip = ""
i = 0
j = 0
k = 1
# Cette boucle va servir de passer de IP == X.X.X.X à IP == X.X.X. (on stock ça dans ip)
for i in range(len(IPAddr)):
    if IPAddr[i] == ".":
        Incr += 1
        ip += IPAddr[i]
        i += 1
    elif Incr == 3:
        break
    else:
        ip += IPAddr[i]
        i += 1
# Là on clear le fichier result sinon c'est pas bo
z = open("result.txt", "w")
z.writelines("")
z.close()
# On ping les IPs de X.X.X.0 à X.X.X.3
# Normalement on est censé le faire de X.X.X.0 à X.X.X.255 pour choper tout le réseau
# Mais vasy c'est long :))
# Cette petite boucle sert surtout à enregistrer toutes les IPs qui ont répondues au ping dans la table ARP.
for i in range(0, 3):
    toto = sr(IP(dst=ip + str(i))/ICMP(), timeout=1)

# Cette partie va aller chercher les infos dans la table ARP et les enregistrer dans result.txt
for j in range(1, len(ARPTABLE)):
    pingr = sr1(IP(dst=ARPTABLE[j]["IP address"])/ICMP())
    v = ""
    host = str(ARPTABLE[j]["IP address"])
    distrib = ""
    # Là tu chope le "num" de ton OS genre si t'as un linux tu chope un 64 ou un 255 ça dépend
    a = IP()
    v = a[IP].ttl
    l = str(v)
    # Là tu mets les ports que tu veux observer dans une liste
    # Pareil là si t'es chaud pour que ton os se tape 65K ports à traiter vasy mec XD
    for i in range(1024):
        port_range.append(i)
    # Voici une petite boucle for qu'on a trouvé sur le google et aussi modifié
    # Lien de la boucle --> https://thepacketgeek.com/scapy/building-network-tools/part-10/
    # Du coup elle sert à vérifier quels ports sont ouvers sur cette IP
    for dst_port in port_range:
        src_port = random.randint(1025, 65534)
        resp = sr1(
            IP(dst=host)/TCP(sport=src_port, dport=dst_port, flags="S"), timeout=0.01,
            verbose=0,
        )
        if resp is None:
            continue

        elif (resp.haslayer(TCP)):
            if (resp.getlayer(TCP).flags == 0x12):
                send_rst = sr(
                    IP(dst=host)/TCP(sport=src_port, dport=dst_port, flags='R'),
                    timeout=1,
                    verbose=0,
                )
                var = f"{host}:{dst_port}"
    # Finalement on construit une phrase avec toutes les infos qu'on a au préalable collecté
    if (l == "255" or l == "64"):
        distrib += "Linux/Macos"
    elif (l == "128"):
        distrib += "Windows"
    result = "ip address = " \
        + (ARPTABLE[j]["IP address"]) \
        + " mac adress = " \
        + (ARPTABLE[j]["HW address"]) \
        + " distribution = " \
        + distrib \
        + " open port = " \
        + var
    # Pi on écrit le tout dans result.txt
    f = open("result.txt", "a")
    f.writelines("\n" + result)
    f.close()
