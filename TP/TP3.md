# TP3 : On va router des trucs
# I. ARP
## 1. Echange ARP
#### **🌞Générer des requêtes ARP**
- J'ai ping et ça marche !
- La route table de Marcel : 
```
ip neigh show
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:02 DELAY
10.3.1.11 dev enp0s8 lladdr 08:00:27:d9:38:9c STALE
```
- Celle de John : 
```
ip neigh show
10.3.1.12 dev enp0s8 lladdr 08:00:27:9d:58:06 REACHABLE
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:02 REACHABLE
```
- L'adresse MAC de John : "`08:00:27:d9:38:9c`".
- Celle de Marcel : "`08:00:27:9d:58:06`".
- On a une commande pour voir la ligne correspondant à une IP spécifique : ***ip neigh show `<IP>`***...
Donc si John veux voir la MAC de Marcel il fait :
```
ip neigh show 10.3.1.12
10.3.1.12 dev enp0s8 lladdr 08:00:27:9d:58:06 STALE
```
- Et si Marcel veut voir la sienne il fait :
```
cat /sys/class/net/*/address
08:00:27:9d:58:06
```
## 2. Analyse de trames
#### **🌞Analyse de trames**
- On vide sa table ARP avec la commande :
```
sudo ip neigh flush all
```
- T'as le ARP request et reply sur `tp2_arp.pcapng`.
# II. Routage
#### **🌞Activation du routage**
```
# On repère la zone utilisée par firewalld, généralement 'public' si vous n'avez pas fait de conf spécifique
$ sudo firewall-cmd --list-all
$ sudo firewall-cmd --get-active-zone

# Activation du masquerading
$ sudo firewall-cmd --add-masquerade --zone=public
$ sudo firewall-cmd --add-masquerade --zone=public --permanent
```
Ui c'est un copié collé...

#### **🌞Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping**
Pour rajouter une route static il faut rajouter **<NETWORK_ADDRESS> via <IP_GATEWAY> dev <LOCAL_INTERFACE_NAME>** au fichier **/etc/sysconfig/network-scripts/route-<INTERFACE_NAME>**
Ensuite on ajoute une route par défaut en ajoutant **GATEWAY=<IP_GATEWAY>** au fichier **/etc/sysconfig/network**

Marcel : 
```
[toto@localhost ~]$ cat /etc/sysconfig/network-scripts/route-enp0s9
10.3.1.0/24 via 10.3.2.254 dev enp0s9

[toto@localhost ~]$ cat /etc/sysconfig/network
# Created by anaconda

GATEWAY=10.3.2.254
```

John : 
```
[toto@localhost ~]$ cat /etc/sysconfig/network-scripts/route-enp0s8
10.3.2.0/24 via 10.3.1.254 dev enp0s8

[toto@localhost ~]$ cat /etc/sysconfig/network
# Created by anaconda

GATEWAY=10.3.1.254
```

Les ping fonctionnent bien :))

## 2. Analyse de trames
#### **🌞Analyse des échanges ARP**

| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP | `john`  `10.3.1.11`       | `john` `AA:BB:CC:DD:EE` | `Gateway` `10.3.1.254`            | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `Gateway` `10.3.1.254`         | `Gateway`   `AA:BB:CC:DD:EE`                    |   `john`  `10.3.1.11`          | `john` `AA:BB:CC:DD:EE`    |
| 3   | Requête ARP         | `marcel`  `10.3.2.12`       | `marcel` `AA:BB:CC:DD:EE`                    |    `Gateway` `10.3.2.254`            |   Broadcast `FF:FF:FF:FF:FF`                         |
| 4   | Réponse ARP         | `Gateway` `10.3.2.254`         | `Gateway`   `AA:BB:CC:DD:EE`                    |   `marcel`  `10.3.2.12`          | `marcel` `AA:BB:CC:DD:EE`    |

## 3. Accès internet
On a déjà donné une route par défault donc on rajoute le NAT à `router` et PAF !! John et Marcel sont connectés à internet !!

Ça fonctionne regarde : 
```
[toto@localhost ~]$ dig gitlab.com

; <<>> DiG 9.16.23-RH <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45014
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;gitlab.com.			IN	A

;; ANSWER SECTION:
gitlab.com.		128	IN	A	172.65.251.78

;; Query time: 17 msec
;; SERVER: 89.2.0.1#53(89.2.0.1)
;; WHEN: Tue Oct 11 23:53:44 CEST 2022
;; MSG SIZE  rcvd: 55
```
1 | ping | john 10.3.1.12 | john AA:BB:CC:DD:EE | 8.8.8.8 | ?

2 |
pong |
8.8.8.8 |
? |
john 10.3.1.12 |
john AA:BB:CC:DD:EE 