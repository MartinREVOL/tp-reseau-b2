# Guide du scanner réseau

## Présentation du scanner
Notre scanner a été developpé en python, il permet d'envoyer des pings vers toutes les adresses IP d'un réseau auquel nous sommes connéctés. Si nous arrivons à ping une adresse nous pourrons trouver dans un fichier certaines informations que nous avons pu récupérer, tel que l'adresse IP, l'adresse MAC, le systeme d'exploitation de la machine détéctée ainsi que les ports ouverts. 

## Comment obtenir et utiliser le scanner
Il faut d'abord installer "`scapy`" (en root bien sûr):
```bash
pip install scapy
```
Si t'as pas "`pip`":
```bash
python3 -m pip install
```
Ensuite chope le repos (sinon tu chope direct le dossier "`scanner`"):
```bash
git clone https://gitlab.com/martinrevol1/tp-reseau-b2.git
```
Il faudra ensuite lancer le scanner avec:
```bash
python3 scanner/Scan.py
```

Et le scanner sera en route. Afin d'observer le compte rendu du scan il faudra ouvrir le fichier `result.txt` qui se trouve dans `scanner/result.txt`.

Ou alors tu tape dans un terminal:
```bash
more scanner/result.txt
```
Et c'est bon, vous aurez tout plein d'informations sur les machines dans votre réseau.

**PS** : N'hesite pas à check le code stv, j'y ai mis plein de commentaires pour mieux comprendre comment il fonctionne (`Scan.py`)...

Voilà voilà tchuss !

![ahah](https://cdn.discordapp.com/attachments/713411498281795665/1033858454634369024/cat-sticker-line-sticker.gif)

**REVOL Martin**

**BRAVO Valentin**
