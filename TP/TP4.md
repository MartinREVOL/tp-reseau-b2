# TP4 : TCP, UDP et services réseau
# I. First steps
Pour cette partie on va prendre `Spotify`, `Discord`, `gitlab.com`, `youtube.com` et `GarageBand`.

### Spotify : 
- Fichier --> [spotify.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark) ;
- On peut voir que c'est un requête `TCP` ;
- Avec une IP du serv --> `199.232.58.248` ;
- Le port du serv --> `443` ;
- Le port local --> `63518` ;

Je demande à mon OS :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo lsof -nP -i TCP | grep Spotify
Spotify   45962 revol-buisson   57u  IPv4 0x6c61b796d8e05b0b      0t0  TCP 10.33.19.110:63518->199.232.58.248:443 (ESTABLISHED)
```
On reconnait toutes les infos trouvé avec **WireShark** !

### Discord :
- Fichier --> [discord.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark) ;
- On peut voir que c'est un requête `TCP` ;
- Avec une IP du serv --> `162.159.135.234` ;
- Le port du serv --> `443` ;
- Le port local --> `64081` ;

Je demande à mon OS :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo lsof -nP -i TCP | grep Discord
Discord   46470 revol-buisson   29u  IPv4 0x6c61b796d27b10e3      0t0  TCP 10.33.19.110:64081->162.159.135.234:443 (ESTABLISHED)
```
On reconnait toutes les infos trouvé avec **WireShark** !

### gitlab.com :
- Fichier --> [gitlab.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark) ;
- On peut voir que c'est un requête `TCP` ;
- Avec une IP du serv --> `172.65.251.78` ;
- Le port du serv --> `443` ;
- Le port local --> `64101` ;

Je demande à mon OS :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo lsof -nP -i TCP | grep com.apple
com.apple 39777 revol-buisson    4u  IPv4 0x6c61b796d27ad3f3      0t0  TCP 10.33.19.110:64101->172.65.251.78:443 (ESTABLISHED)
```
On reconnait toutes les infos trouvé avec **WireShark** !

### youtube.com :
- Fichier --> [youtube.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark) ;
- On peut voir que c'est un requête `TCP` ;
- Avec une IP du serv --> `216.58.213.78` ;
- Le port du serv --> `443` ;
- Le port local --> `64139` ;

Je demande à mon OS :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo lsof -nP -i TCP | grep com.apple
com.apple 39770 revol-buisson   15u  IPv4 0x6c61b796d27ade1b      0t0  TCP 10.33.19.110:64139->216.58.213.78:443 (ESTABLISHED)
```
On reconnait toutes les infos trouvé avec **WireShark** !

### GarageBand :
- Fichier --> [garageband.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark) ;
- On peut voir que c'est un requête `TCP` ;
- Avec une IP du serv --> `17.253.109.201` ;
- Le port du serv --> `443` ;
- Le port local --> `64257` ;

Je demande à mon OS :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo lsof -nP -i TCP | Garage
GarageBan 47244 revol-buisson   32u  IPv4 0x6c61b796cd7aae1b      0t0  TCP 10.33.19.110:64257->17.253.109.201:443 (ESTABLISHED)
```
On reconnait toutes les infos trouvé avec **WireShark** !

# II. Mise en place
## 1. SSH
On démarre notre VM et on se connecte dessus en SSH :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % ssh toto@10.3.1.11
toto@10.3.1.11's password: 
Last login: Sun Oct 16 19:49:52 2022
```
- On peut voir dans le fichier [tp4_ssh_traffic.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark) que c'est bien du trafic TCP qu'il y a avec SSH, pour la simple et bonne raison que TCP est bien plus "SAFE" que UDP.
- On a le 3-Way Handshake à l'établissement de la connexion --> [tp4_SYN-SYNACK-ACK.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark).
- On a aussi le FIN FINACK à la fin d'une connexion --> [tp4_FIN-FINACK.pcapng](https://gitlab.com/martinrevol1/tp-reseau-b2/-/tree/main/Wireshark).

Je demande à mon OS : 
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo lsof -nP -i TCP
ssh       5219   revol-buisson    3u  IPv4 0x2983479368f661f5      0t0  TCP 10.3.1.1:51848->10.3.1.11:22 (ESTABLISHED)
```
Et sur ma VM :
```
[toto@localhost ~]$ ss
tcp               ESTAB              0                  0                                                 10.3.1.11:ssh                                     10.3.1.1:51848
```

Et BANG ! Tout correspond quel bonheur !

## 2. NFS
### 🌞 Mettez en place un petit serveur NFS sur l'une des deux VMs
- Pour ça j'ai d'abord suivi la doc [là](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=1) ;
- 
