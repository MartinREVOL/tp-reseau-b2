# TP2 : Ethernet, IP, et ARP
# I. Setup IP
#### **🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines**
- j'ai choisi les IP **192.168.34.2/26** et **192.168.34.15/26**
- avec adresse réseau --> **192.168.34.0/26**
- et broadcast --> **192.168.34.63/26**

Alors... Vu que je fais avec une VM, j'ai eu besoin de modifier l'adresse IP de ma VM... Du coup j'ai fais :
```
ifconfig enp0s8 192.168.34.15 netmask 255.255.255.192
```
On pourra d'ailleurs voir sur le fichier [gadca.pcapng](../Wireshark//gadca.pcapng), que les ping sont des ICPM de type "echo request" et "echo reply".

# II. ARP my bro
#### **🌞 Check the ARP table**
La commande pour afficher la table ARP --> ***arp -a*** :
```
arp -a           
? (192.168.0.1) at 0:37:b7:50:67:3 on en0 ifscope [ethernet]
? (192.168.0.5) at 0:37:b7:50:67:4 on en0 ifscope [ethernet]
? (192.168.0.255) at ff:ff:ff:ff:ff:ff on en0 ifscope [ethernet]
? (192.168.34.15) at 8:0:27:5a:f4:fc on vboxnet0 ifscope [ethernet]
? (192.168.34.63) at ff:ff:ff:ff:ff:ff on vboxnet0 ifscope [ethernet]
? (224.0.0.251) at 1:0:5e:0:0:fb on en0 ifscope permanent [ethernet]
? (239.255.255.250) at 1:0:5e:7f:ff:fa on en0 ifscope permanent [ethernet]
```
Après tu regarde la table --> ? (192.168.34.15) at ```8:0:27:5a:f4:fc``` on vboxnet0 ifscope [ethernet]
Et enfin la gateway tu fais --> ***route get default*** (comme ça tu chope son IP) :
```
gateway: 192.168.0.1
```
Pi ensuite tu regarde la table --> ? (192.168.0.1) at ```0:37:b7:50:67:3``` on en0 ifscope [ethernet]
#### 🌞 **Manipuler la table ARP**
Pour vider ma table ARP --> **sudo arp -a -d** :
```
sudo arp -a -d
Password:
192.168.0.1 (192.168.0.1) deleted
192.168.0.255 (192.168.0.255) deleted
192.168.34.15 (192.168.34.15) deleted
192.168.34.63 (192.168.34.63) deleted
224.0.0.251 (224.0.0.251) deleted
239.255.255.250 (239.255.255.250) deleted
```
Donc si je refais **arp -a** ça donne :
```
arp -a
? (192.168.0.1) at 0:37:b7:50:67:3 on en0 ifscope [ethernet]
? (192.168.0.255) at ff:ff:ff:ff:ff:ff on en0 ifscope [ethernet]
? (192.168.34.63) at ff:ff:ff:ff:ff:ff on vboxnet0 ifscope [ethernet]
? (239.255.255.250) at 1:0:5e:7f:ff:fa on en0 ifscope permanent [ethernet]
```
Je re-ping du coup et ça donne :
```
arp -a
? (192.168.0.1) at 0:37:b7:50:67:3 on en0 ifscope [ethernet]
? (192.168.0.255) at ff:ff:ff:ff:ff:ff on en0 ifscope [ethernet]
? (192.168.34.15) at 8:0:27:5a:f4:fc on vboxnet0 ifscope [ethernet]
? (192.168.34.63) at ff:ff:ff:ff:ff:ff on vboxnet0 ifscope [ethernet]
? (224.0.0.251) at 1:0:5e:0:0:fb on en0 ifscope permanent [ethernet]
? (239.255.255.250) at 1:0:5e:7f:ff:fa on en0 ifscope permanent [ethernet]
```
#### 🌞 **Wireshark it**
On peux voir avec le fichier [arp-first-time.pcapng](../Wireshark/arp-first-time.pcapng) que c'est l'**IP** de mon **PC** qui demande qui est l'adresse **IP** de ma **VM**, puis ma **VM** réponds sa **MAC**. Et inversement !!!
# III. DHCP you too my brooo
#### 🌞 Wireshark it
On peux voir sur lr fichier [dhcp-new.pcapng](../Wireshark/dhcp-new.pcapng) qu'on a :
- 1ere trame avec un **`D`iscover** qui a pour source 0.0.0.0 et destination 255.255.255.255
- 2eme trame avec un **`O`ffer** qui a pour source 192.168.0.1 et pour destination 192.168.0.11
- 3eme trame (en double jsp pourquoi) avec un **`R`equest** qui a pour source 0.0.0.0 et destination 255.255.255.255
- 4eme trame avec un **`A`cknowledge** qui a pour source 192.168.0.1 et pour destination 192.168.0.11

Voilà donc la fameuse `DORA` :)))

Je poursuit :
- IP proposée : 192.168.0.11
- IP de la passerelle : 192.168.0.1
- l'adresse d'un serveur DNS : pour ça tu vas chercher dans les infos en dessous : 
    - Dynamique Host Configuration Protocol (Discover) ; 
    - Option : (6) Domain Name Server ;
    - Et tu trouve deux adresses (soit 89.2.0.1 soit 89.2.0.2)

# IV. Avant-goût TCP et UDP
Nikel ! ALors... Quand je regarde une vidéo ytb mon PC se connecte à l'adresse IP **216.58.215.46** et au port TCP **443** comme indiqué dans le fichier [TCP-UCP.pcapng](../Wireshark/TCP-UCP.pcapng)...

Voilà voilà c'était cool ce TP bisous :3❤️

