# TP1 réseau
**Je précise que je suis sur mac --> donc je n'ai pas de prise éthernet --> donc j'ai fais la partie II et IV avec Valentin et Florian (c'est pour ça que ces parties sont faites pour Windows).**
# I. Exploration locale en solo
# 1. Affichage d'informations sur la pile TCP/IP locale
### En ligne de commande
#### 🌞 Affichez les infos des cartes réseau de votre PC
On tape la commande ***networksetup -listallhardwareports*** pour faire afficher le nom de l'interface wifi et l'adresse MAC :
```
Hardware Port: Wi-Fi
Device: en0
Ethernet Address: 3c:22:fb:78:83:38
```
Et ***ifconfig*** pour avoir l'adresse ip (mais vu que je suis un bg j'ai fais ***ifconfig | grep "inet" | grep -v 127.0.0.1 | grep netmask*** comme ça je suis direct tombé dessus hihi) :
```
inet 10.33.19.110 netmask 0xfffffc00 broadcast 10.33.19.255
```

Ah ui j'allais oublier j'ai un mac tout pourri donc pas de carte ethernet voilà ;-;
#### 🌞 Affichez votre gateway
Bah utilise ***route get default*** et t'auras  l'adresse IP de la passerelle de ta carte WiFi (avec un piti grep pour les bg --> ***route get default | grep gateway***) :
```
gateway: 10.33.19.254
```
### En graphique (GUI : Graphical User Interface)
#### 🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)
Pour la gateway tu m'as dit non 🤷🏻‍♂️, mais sinon pour l'ip tu clique sur la pomme, "préférence système", "Réseau"... Et l'adresse mac tu fais la pomme, "à propos de ce mac", "rapport système...", "Réseau", "Configuration", "WIFI"...
Et voilà :
```
Wi-Fi est connecté à WiFi@YNOV et possède l’adresse IP 10.33.19.110.
Adresse matérielle (MAC) :	3c:22:fb:78:83:38
```
### Questions
#### 🌞 à quoi sert la gateway dans le réseau d'YNOV ?
C'est pour permettre la connection entre le réseau d'YNOV et mon PC via des mp entre les deux ;)
![gateway](https://docs.citrix.com/en-us/citrix-gateway-service/media/xaxd_1_new.png)
# 2. Modifications des informations
## A. Modification d'adresse IP (part 1)
#### 🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP
Bon pour mac tu fais ça :
- Sélectionnez Préférences Système.
- Choisissez Réseau.
- Sélectionnez une interface de réseau active dans le menu déroulant.
- Veuillez remarquer quelle est votre adresse IP actuelle afin d'effectuer des modifications informées.
- Cliquez sur le bouton Avancés.
- Sélectionnez TCP/IP.
- Sélectionnez Manuellement dans le menu Configurer IPv4.
- Saisissez une adresse IP statique dans le champ Adresse IPv4.
- Cliquez sur OK et cliquez sur Appliquer.
![Voilà](https://scontent.cdninstagram.com/v/t51.39111-15/309000454_622594559361969_4148988549278125966_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=5a057b&_nc_ohc=rdN2Pobe898AX-X6u7j&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.cdninstagram.com&oh=02_ARrRgYMUh-ICdenfnKp1Bo7LoAyXgEk9EQzItevtn9Jfcw&oe=6337FBE2)
#### 🌞 Il est possible que vous perdiez l'accès internet.
Effectivement, il est possible de perdre la connection internet après modification d'IP, le problème se manifeste si t'a pris une IP dans un sous réseau différent (192.168.10.0/24 au lieu de 192.168.1.0/24).
Si tu veux essayer de changer ton adresse IP, tu peux changer que le dernier octet (le dernier nombre) pour rester dans le même réseau que ta passerelle.
# II. Exploration locale en duo

Owkay. Vous savez à ce stade :

- afficher les informations IP de votre machine
- modifier les informations IP de votre machine
- c'est un premier pas vers la maîtrise de votre outil de travail

On va maintenant répéter un peu ces opérations, mais en créant un réseau local de toutes pièces : entre deux PCs connectés avec un câble RJ45.

# 1. Prérequis

- deux PCs avec ports RJ45
- un câble RJ45
- **firewalls désactivés** sur les deux PCs

# 2. Câblage

Ok c'est la partie tendue. Prenez un câble. Branchez-le des deux côtés. **Bap.**

# Création du réseau (oupa)

Cette étape peut paraître cruciale. En réalité, elle n'existe pas à proprement parlé. On ne peut pas "créer" un réseau. Si une machine possède une carte réseau, et si cette carte réseau porte une adresse IP, alors cette adresse IP se trouve dans un réseau (l'adresse de réseau). Ainsi, le réseau existe. De fait.  

**Donc il suffit juste de définir une adresse IP sur une carte réseau pour que le réseau existe ! Bap.**

# 3. Modification d'adresse IP

#### 🌞 Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- modifiez l'IP des deux machines pour qu'elles soient dans le même réseau
  - choisissez une IP qui commence par "192.168"
  ```bash
     Adresse IPv4. . . . . . . . . . . . . .: 192.168.137.5(préféré)
  ```
  - utilisez un /30 (que deux IP disponibles)
- vérifiez à l'aide de commandes que vos changements ont pris effet
- utilisez `ping` pour tester la connectivité entre les deux machines

```bash
PS C:\Users\bravo> ping 192.168.137.2

Envoi d’une requête 'Ping'  192.168.137.2 avec 32 octets de données :
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.137.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
```
	
- affichez et consultez votre table ARP
**PS C:\Users\bravo> arp -a**

```bash
Interface : 169.254.240.247 --- 0xa
  Adresse Internet      Adresse physique      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.2             01-00-5e-00-00-02     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  230.0.0.1             01-00-5e-00-00-01     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.137.5 --- 0x11
  Adresse Internet      Adresse physique      Type
  192.168.137.2         7c-d3-0a-82-b9-c5     dynamique
  192.168.137.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
# 4. Utilisation d'un des deux comme gateway

Ca, ça peut toujours dépann. Comme pour donner internet à une tour sans WiFi quand y'a un PC portable à côté, par exemple. 

L'idée est la suivante :

- vos PCs ont deux cartes avec des adresses IP actuellement
  - la carte WiFi, elle permet notamment d'aller sur internet, grâce au réseau YNOV
  - la carte Ethernet, qui permet actuellement de joindre votre coéquipier, grâce au réseau que vous avez créé :)
- si on fait un tit schéma tout moche, ça donne ça :


- pour ce faiiiiiire :
  - désactivez l'interface WiFi sur l'un des deux postes
  - s'assurer de la bonne connectivité entre les deux PCs à travers le câble RJ45
  - **sur le PC qui n'a plus internet**
    - sur la carte Ethernet, définir comme passerelle l'adresse IP de l'autre PC
  - **sur le PC qui a toujours internet**
    - sur Windows, il y a une option faite exprès (google it. "share internet connection windows 10" par exemple)
    - sur GNU/Linux, faites le en ligne de commande ou utilisez [Network Manager](https://help.ubuntu.com/community/Internet/ConnectionSharing) (souvent présent sur tous les GNU/Linux communs)
    - sur MacOS : toute façon vous avez pas de ports RJ, si ? :o (google it sinon)

---

- 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu
 - encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait
 
 **PS C:\Users\bravo> ping 8.8.8.8**
 
```bash
 Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=113
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=113
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=113
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=113

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 23ms, Maximum = 23ms, Moyenne = 23ms
 ```
- 🌞 utiliser un `traceroute` ou `tracert` pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

**PS C:\Users\bravo> tracert -4 192.168.137.2**

```bash
Détermination de l’itinéraire vers DESKTOP-F1QHIV8 [192.168.137.2]
avec un maximum de 30 sauts :

  1     1 ms     1 ms     1 ms  DESKTOP-F1QHIV8 [192.168.137.2]

Itinéraire déterminé.
```
# 5. Petit chat privé

On va créer un chat extrêmement simpliste à l'aide de `netcat` (abrégé `nc`). Il est souvent considéré comme un bon couteau-suisse quand il s'agit de faire des choses avec le réseau.

Sous GNU/Linux et MacOS vous l'avez sûrement déjà, sinon débrouillez-vous pour l'installer :). Les Windowsien, ça se passe [ici](https://eternallybored.org/misc/netcat/netcat-win32-1.11.zip) (from https://eternallybored.org/misc/netcat/).  

Une fois en possession de `netcat`, vous allez pouvoir l'utiliser en ligne de commande. Comme beaucoup de commandes sous GNU/Linux, Mac et Windows, on peut utiliser l'option `-h` (`h` pour `help`) pour avoir une aide sur comment utiliser la commande.  

Sur un Windows, ça donne un truc comme ça :



L'idée ici est la suivante :

- l'un de vous jouera le rôle d'un *serveur*
- l'autre sera le *client* qui se connecte au *serveur*

Précisément, on va dire à `netcat` d'*écouter sur un port*. Des ports, y'en a un nombre fixe (65536, on verra ça plus tard), et c'est juste le numéro de la porte à laquelle taper si on veut communiquer avec le serveur.

Si le serveur écoute à la porte 20000, alors le client doit demander une connexion en tapant à la porte numéro 20000, simple non ?  

Here we go :

- 🌞 **sur le PC *serveur*** avec par exemple l'IP 192.168.1.1
**Je suis PC client**
- 🌞 **sur le PC *client*** avec par exemple l'IP 192.168.1.2
  - `nc.exe 192.168.1.1 8888`
    - "`netcat`, connecte toi au port 8888 de la machine 192.168.1.1 stp"
	
```bash
C:\Users\bravo\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe 192.168.137.2 8888
```
  - une fois fait, vous pouvez taper des messages dans les deux sens
  
  ```bash
  a
b
OE OEOE
```
- appelez-moi quand ça marche ! :)

---

# 6. Firewall

Toujours par 2.

Le but est de configurer votre firewall plutôt que de le désactiver

- Activez votre firewall
- 🌞 Autoriser les `ping`

**avant que la regle ne soit mise en place**

```bash
PS C:\Users\bravo> ping 192.168.137.2

Envoi d’une requête 'Ping'  192.168.137.2 avec 32 octets de données :
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.

Statistiques Ping pour 192.168.137.2:
    Paquets : envoyés = 4, reçus = 0, perdus = 4 (perte 100%),
```
  - configurer le firewall de votre OS pour accepter le `ping`

**apres la mise en place**

![](https://cdn.discordapp.com/attachments/1024255244252753951/1024615418503106570/bjdqghbjgqhj.PNG)

![](https://cdn.discordapp.com/attachments/1024255244252753951/1024615418947711016/cxjkqjblkg.PNG)


```bash
PS C:\Users\bravo> ping 192.168.137.2

Envoi d’une requête 'Ping'  192.168.137.2 avec 32 octets de données :
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.137.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 1ms, Moyenne = 1ms
```
- 🌞 Autoriser le traffic sur le port qu'utilise `nc`

![](https://cdn.discordapp.com/attachments/1024255244252753951/1024619948871528519/unknown.png)
  - vous utiliserez ce port pour [communiquer avec `netcat`](#5-petit-chat-privé-) par groupe de 2 toujours
  
  ```bash
  C:\Users\bravo\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe 192.168.137.2 8888
h
a
```
  - le firewall du *PC serveur* devra avoir un firewall activé et un `netcat` qui fonctionne
  
  **yep**
  
# III. Manipulations d'autres outils/protocoles côté client
# 1. DHCP
#### 🌞 Exploration du DHCP, depuis votre PC
Pour afficher l'adresse IP du serv DHCP tu tapa la commande ***ipconfig getoption en0 server_identifier*** :
```
ipconfig getoption en0 server_identifier
10.33.19.254
```
Et sur mac on a pas la date on a la durée du bail en sec :
```
ipconfig getoption en0 lease_time
172800
```
# 2. DNS
#### 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur
Tu tape ***scutil --dns | grep nameserver | sort -u*** :
```
scutil --dns | grep nameserver | sort -u
  nameserver[0] : 8.8.8.8
  nameserver[1] : 8.8.4.4
  nameserver[2] : 1.1.1.1
```
#### 🌞 utiliser, en ligne de commande l'outil nslookup pour faire des requêtes DNS à la main.
Pour google.com : ***nslookup google.com***
```
nslookup google.com

Server:		8.8.8.8
Address:	8.8.8.8#53

Name:	google.com
Address: 216.58.215.46
```
Pour ynov.com : ***nslookup ynov.com***
```
nslookup ynov.com

Server:		8.8.8.8
Address:	8.8.8.8#53

Name:	ynov.com
Address: 104.26.11.233
Name:	ynov.com
Address: 172.67.74.226
Name:	ynov.com
Address: 104.26.10.233
```
Comme on peux le voir on a le nom et l'adresse IP du serveur x)
On peux aussi voir que les requêtes ont été envoyé au serveur 8.8.8.8 (google).

Pour 78.74.21.21 : ***nslookup 78.74.21.21***
```
nslookup 78.74.21.21               
Server:		8.8.8.8
Address:	8.8.8.8#53

Non-authoritative answer:
21.21.74.78.in-addr.arpa	name = host-78-74-21-21.homerun.telia.com.
```
On trouve ce truc --> host-78-74-21-21.homerun.telia.com

Pour 92.146.54.88 : ***nslookup 92.146.54.88***
```
nslookup 92.146.54.88
Server:		8.8.8.8
Address:	8.8.8.8#53

** server can't find 88.54.146.92.in-addr.arpa: NXDOMAIN
```
On trouve pas de serveur T^T

# IV. Wireshark
Wireshark est un outil qui permet de visualiser toutes les trames qui sortent et entrent d'une carte réseau.

Il peut :

- enregistrer le trafic réseau, pour l'analyser plus tard
- afficher le trafic réseau en temps réel

On peut TOUT voir.

Un peu austère aux premiers abords, une manipulation très basique permet d'avoir une très bonne compréhension de ce qu'il se passe réellement.

- téléchargez l'outil [Wireshark](https://www.wireshark.org/)
- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
  - un ping entre vous et la passerelle

  ![](https://cdn.discordapp.com/attachments/1024255244252753951/1024622675609522206/1652626.PNG)

  - un netcat entre vous et votre mate, branché en RJ45

  ![](https://cdn.discordapp.com/attachments/905799668938723329/1024623318424358968/333.PNG)
  - une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.

  ![](https://cdn.discordapp.com/attachments/905799668938723329/1024624002796376114/666.PNG)

  8888 correspond à Google.com

  ## Voilà voilà c'est la fin bisous :3 ❤️

  ![err](https://cdn.discordapp.com/attachments/1024255244252753951/1024628831019081730/Capture_decran_2022-09-28_a_12.28.41.png)
